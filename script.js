

function filterBy(arr, type) {
  return arr.filter(el => typeof(el) !== type);
};

console.log(filterBy(['hello', 'world', 23, '23', null, undefined, 55, 'Anton', NaN], 'number'));

